extends Node

var Server
var spawns = []

var player = load("res://Actors/Player.tscn")
var combattroop  = load("res://Actors/CombatTroop.tscn")

func LoadPlayerObjects(player_id):
	var playersprite = player.instance()
	playersprite.add_to_group(str(player_id))
	for i in spawns.size():
		if spawns[i].get_child_count() == 0:
			spawns[i].add_child(playersprite)
			Server.initialize_player(player_id,spawns[i].global_position)
			break

func LoadCombatTroop(player_id,pos,group):
	var troopsprite = combattroop.instance()
	troopsprite.add_to_group(group)
	get_tree().get_nodes_in_group(var2str(player_id))[0].add_child(troopsprite)
	troopsprite.global_position = pos
	pass
	
	
