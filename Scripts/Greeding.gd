extends Node2D

#### WORK IN PROGRESS ####

var greedhut = preload("res://Actors/Greedhut.tscn")

# stats
var level : int  = 1
var power : int = level*5
var hp : int = level*5


# harvesting related
var harvest_chance = 4
var currently_carrying = 0
var res_per_harvest = 100
var capacity = 600

# hut data
var has_hut = false
var my_hut

func _on_Timer_timeout():
	#Harvest-Build
	#temp parent
	var grandpa = get_parent().get_parent()
	if randi() % harvest_chance == 1 && grandpa.resource_amount > res_per_harvest:
		grandpa.resource_amount -= 100
		currently_carrying += 100
		grandpa.update_resource_amount()
		if currently_carrying == capacity && !has_hut:
			currently_carrying = 0
			var greed_hut = greedhut.instance()
			my_hut = greed_hut
			Global.world.add_child(greed_hut)
			greed_hut.global_position = global_position + Vector2(rand_range(-100,100),rand_range(-100,100))
			greed_hut.resource_amount = capacity
			# spawn building
			has_hut = true
			pass
		elif currently_carrying == capacity && has_hut:
			currently_carrying = 0
			my_hut.resource_amount += capacity
			my_hut.update_hut()
			pass
	else:
		pass
