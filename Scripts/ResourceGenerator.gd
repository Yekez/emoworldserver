extends Node2D

var resource = preload("res://Scenes/Mineable.tscn")

var x
var y
var _x
var _y
var max_res = 20
var init_max_distance = 2000
var max_distance = 6500
var mineable_id = 0

func _ready():
	randomize()
#	Global.closest_resource_pos = $Mineable2.position
#	Global.resource_generator = self

# Version 2
func instance_resources():
	x = rand_range(-max_distance,max_distance)
	y = rand_range(-max_distance,max_distance)
	var level = Global.resource_level[randi() % Global.resource_level.size()]
	var spawn_location = Vector2(x,y)
	var distance_to_base = spawn_location.distance_to(Vector2(0,0))
	if get_tree().get_nodes_in_group("mineables").size() < max_res:
		# Distance based modifier
		if distance_to_base>1200 && distance_to_base<=2500 && level==1:
			var resource1 = resource.instance()
			resource1.resource_level = level
			resource1.name = var2str(mineable_id)
			mineable_id += 1
			add_child(resource1)
			resource1.position = spawn_location
			get_node("/root/Server").send_mineable_data(spawn_location, level, resource1.name)

		if distance_to_base>2500 && distance_to_base<=3500 && level==2:
			var resource2 = resource.instance()
			resource2.resource_level = level
			resource2.name = var2str(mineable_id)
			mineable_id += 1
			add_child(resource2)
			resource2.position = spawn_location 
			get_node("/root/Server").send_mineable_data(spawn_location, level, resource2.name)

		if distance_to_base>3500 && distance_to_base<=4500 && level==3:
			var resource3 = resource.instance()
			resource3.resource_level = level
			resource3.name = var2str(mineable_id)
			mineable_id += 1
			add_child(resource3)
			resource3.position = spawn_location 
			get_node("/root/Server").send_mineable_data(spawn_location, level, resource3.name)

		if distance_to_base>4500 && distance_to_base<=5500 && level==4:
			var resource4 = resource.instance()
			resource4.resource_level = level
			resource4.name = var2str(mineable_id)
			mineable_id += 1
			add_child(resource4)
			resource4.position = spawn_location 
			get_node("/root/Server").send_mineable_data(spawn_location, level, resource4.name)

		if distance_to_base>5500 && distance_to_base<=6500 && level==5:
			var resource5 = resource.instance()
			resource5.resource_level = level
			resource5.name = var2str(mineable_id)
			mineable_id += 1
			add_child(resource5)
			resource5.position = spawn_location 
			get_node("/root/Server").send_mineable_data(spawn_location, level, resource5.name)



func _on_Timer_timeout():
	if get_child_count() < 50:
		instance_resources()

