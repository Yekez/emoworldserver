extends Node2D

var luck_factor = 2
var area_lvl : int
var enemy_power = 0
var enemy_hp = 0
var ally_power = 0
var ally_hp = 0
var Greeding = preload("res://Actors/Greeding.tscn")


func _ready():
	area_lvl = get_parent().resource_level
	if area_lvl < 3:
		queue_free()
	spawn_monsters()

func spawn_monsters():
	if area_lvl > 2:
		for i in area_lvl:
			var greeding = Greeding.instance()
			call_deferred("add_child", greeding)
			greeding.position = Vector2(rand_range(-130,130),rand_range(-130,130))
			enemy_power += greeding.power
			enemy_hp += greeding.hp
			# Consider adding enemy levels
	pass
