extends Node2D

var resource_level = 1
var base_amount = 1000
var harvest_requester_id : int
var reward : int
var resource_amount : int 

func _ready():
	resource_amount = base_amount*resource_level*resource_level
	pass

func harvest_started(dur,id):
	$HarvestTimer.start(dur)
	harvest_requester_id = id
	pass


func _on_HarvestTimer_timeout():
	reward = resource_amount
	get_node("/root/Server").mineable_harvested(name,reward,harvest_requester_id)
	queue_free()
	pass # Replace with function body.

func update_resource_amount() -> void:
	get_tree().get_node("/root/Server").update_resource_amount(name,resource_amount)
	pass
