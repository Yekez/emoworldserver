extends Node2D

var network = NetworkedMultiplayerENet.new()
var port = 6007
var max_players = 4000


func _ready():
	StartServer()
	InitializePlayer.Server = self
func StartServer():
	network.create_server(port, max_players)
	get_tree().set_network_peer(network)
	print("Server Started")
	pass
	
	network.connect("peer_connected", self, "_Peer_Connected")
	network.connect("peer_disconnected", self, "_Peer_Disconnected")

func _Peer_Connected(player_id):
	print("User " + str(player_id) + " Connected")
	InitializePlayer.LoadPlayerObjects(player_id)
	send_previous_mineable_data(player_id)
	if get_tree().get_network_connected_peers().size()>1:
		for peer_id in get_tree().get_network_connected_peers().size()-1:
			var id = get_tree().get_network_connected_peers()[peer_id]
			rpc_id(id, "initialize_peer", get_tree().get_network_connected_peers()[get_tree().get_network_connected_peers().size()-1])
			rpc_id(get_tree().get_network_connected_peers()[get_tree().get_network_connected_peers().size()-1], "initialize_peer", id)

func _Peer_Disconnected(player_id):
	print("User " + str(player_id) + " Disconnected")
	get_tree().get_nodes_in_group(str(player_id))[0].queue_free()
	for peer_id in get_tree().get_network_connected_peers().size():
		rpc_id(peer_id, "remove_peer", player_id)

func _physics_process(delta) -> void:
	#print(delta)
	for peer_id in get_tree().get_network_connected_peers().size():
		var id = get_tree().get_network_connected_peers()[peer_id]
		send_player_position(id)
		send_countdown_time(id)

func initialize_player(id,spawn_pos):
	rpc_id(id, "initialize_me", spawn_pos)
	print("player ",id," initialized at ", spawn_pos)
	
remote func recieve_player_position(position):
	var player_id = get_tree().get_rpc_sender_id()
	Position.update_player_position(player_id, position)

func send_player_position(id):
	for i in get_tree().get_network_connected_peers().size():
		var peer_id = get_tree().get_network_connected_peers()[i]
		if id != peer_id:
			var pos = get_tree().get_nodes_in_group(str(peer_id))[0].global_position
			rpc_unreliable_id(id, "fetch_peer_position", peer_id, pos)

# I started to think maybe sending all info with one function
# would be better. This is getting out of control really fast xD

remote func initialize_troop(pos,group):
	InitializePlayer.LoadCombatTroop(get_tree().get_rpc_sender_id(),pos,group)
	pass

func send_countdown_time(id):
	var time = round($GlobalCountdownTimer.time_left)
	rpc_id(id, "fetch_global_countdown_timer", time)
	pass

# Find a better way. Did this because godot cant read time == 0 for some reason...
func _on_GlobalCountdownTimer_timeout():
	for peer_id in get_tree().get_network_connected_peers().size():
		var id = get_tree().get_network_connected_peers()[peer_id]
		rpc_unreliable_id(id, "fetch_global_countdown_timeout")


########## Mineable Communication ##########

func send_previous_mineable_data(id):
	# This function needs to be called when a player joins the game.
	# Then it will send all currently spawned mineable data to the player
	var generator = $Game/WorldMap/ResourceGenerator
	var pos
	var level
	var newname
	for res in generator.get_child_count():
		if res != 0:
			pos = generator.get_child(res).global_position
			level = generator.get_child(res).resource_level
			newname = generator.get_child(res).name
			rpc_unreliable_id(id,"fetch_previous_mineable_data",pos,level,newname)
	pass

func send_mineable_data(mineable_position : Vector2, level : int,name):
	rpc_unreliable("fetch_mineable_data", mineable_position, level, name)
	print("mineable data sent")

remote func mineable_harvest_started(mineable_name,dur):
	var generator = $Game/WorldMap/ResourceGenerator
	if generator.get_node(mineable_name):
		generator.get_node(mineable_name).harvest_started(dur,get_tree().get_rpc_sender_id())
		print("Harvest started at Mineable " + str(mineable_name) + ". Duration: " + str(dur))
	pass

func mineable_harvested(name,reward,id) -> void:
	# If a player quits before harvest is completed, this will throw unexisting ID error. 
	# It doesn't crash but an error is an error..
	rpc_id(id,"mineable_harvested",reward) 
	#make an rpc call to remove from all clients
	rpc("remove_mineable",name)
	pass

func update_resource_amount(name,resource_amount) -> void:
	rpc("update_resource_amount", name, resource_amount)
	pass

